import time,random
while True:
    time1 = int(round(time.time()))
    t = random.randint(1,5)
    print('doing some work for {} seconds'.format(t))
    time.sleep(t)
    time2 = int(round(time.time()))
    i=0
    rem = 10 - (time2 -time1)
    print('Idle for {} seconds'.format(rem), end='')
    while (i< rem):
        print('.',end='')
        time.sleep(1)
        i+=1
    print('')

input()