import os, time, threading, datetime
from random import randint

global lights, temp, humid, pump
lights, temp, humid, pump = (1,20,50,1)
def checkTemp(temp = temp):
    i=randint(-3,3)
    temp += i
    return temp

def checkHum(humid = humid):
    i=randint(-3,3)
    humid += i
    return humid

def startPump():
    pass

def lightsOn():
    pass

def lightsOff():
    pass

def writeFile():
    fileTest = open("Riaan.txt","a")
    fileTest.write("Lekker")
    file_object = open(datetime.datetime.now().strftime("%Y-%m-%d")+".txt","a")
    file_object.write(str(datetime.datetime.now()) + " T: " + str(checkTemp()) + " H: " + str(checkHum()) + "\n")
                      


class WorkerThread(threading.Thread):
    def __init__(self):
        super(WorkerThread, self).__init__()
        self.stoprequest = threading.Event()

    def run(self):
        print("Service Started")
        while not self.stoprequest.isSet():
            writeFile()
            time.sleep(1)

    def join(self, timeout=None):
        self.stoprequest.set()
        super(WorkerThread, self).join(timeout)

  