#import RPi.GPIO as GPIO
import RPi.GPIO as GPIO
import time


OP = 2

def pinON(pin):
    if not (GPIO.input(pin)):
        GPIO.output(pin,True)

def pinOFF(pin):
    if (GPIO.input(pin)):
        GPIO.output(pin,False)
try:
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(OP,GPIO.OUT, initial = GPIO.LOW)
    print(GPIO.input(OP))
    pinON(OP)
    print(GPIO.input(OP))
    time.sleep(3)
    pinOFF(OP)
    print(GPIO.input(OP))
except Exception as ex:
    print(ex)

input()


    