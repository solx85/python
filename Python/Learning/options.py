import subprocess as sp
import sys
from threadCode import *


menusDict = {'x': "Main Menu, please make your selection, \n(1) - Start/Stop Service\n(2) - Show Status\n(9) - Exit", 'y': "\n(1) - Back to Main Menu\n(2) - Rerfresh\n(9) - Exit"}
exitBool = False
service1 = WorkerThread()

def showMenus(i='x'):
    tmp = sp.call('cls',shell=True)
    print(menusDict.get(i))

def option1():
  
    print("You have chosen option one!")
    if service1.isAlive():
        print("service already running")
        print("Do you want to stop the service AND exit? \n(y/n)")
        if input() == 'y':
            print("Stopping services...")
            service1.join()
            print("Services stopped")
            sys.exit()
        showMenus()
        
        
    else:
        service1.start()
        showMenus()

    
def option2():
    tmp = sp.call('cls',shell=True)
    print("Status...")
    s = "Running" if service1.is_alive() else "Stopped"
    print("Service : " + s)
    print("Current temprature is: " + str(checkTemp()))
    print("Current humidity is: " + str(checkHum()))
    print("Current light state is: " + str(lights))
    print("Current pump state is: " + str(pump))
    input()
    showMenus()
      

def option9():
    print("Are you sure you want to exit?")
    if input() == 'y':
        z = service1.isAlive()
        if z:
            print("Stopping services...")
            service1.join()
            print("Services stopped")

        sys.exit()
    else:
        showMenus()
    
def optionx():
    showMenus()
    print("Please select a valid menu option")

  
    
mySwitch = {1: option1, 2: option2, 9: option9}