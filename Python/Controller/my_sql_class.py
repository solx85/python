import sqlite3

from texts import *

class mySQLclass:
    
    
    
    def __init__(self,dbname):
        self.db = sqlite3.connect(dbname)
    
    def initializeDB(self):
        print(td[11])
        #self.db.execute('CREATE TABLE IF NOT EXISTS pump(eventtime DATETIME NOT NULL DEFAULT current_timestamp, status TINYINT NOT NULL DEFAULT 0)')
        #self.db.execute('INSERT INTO pump (eventtime) SELECT current_timestamp where not EXISTS (select * from pump)')
        self.db.execute('CREATE TABLE IF NOT EXISTS log(id INTEGER PRIMARY KEY AUTOINCREMENT,eventtime DATETIME, details text)')
        self.db.execute('CREATE TABLE IF NOT EXISTS data(id INTEGER PRIMARY KEY AUTOINCREMENT,eventtime DATETIME,temp text DEFAULT null,humid text DEFAULT null,lights TINYINT,pump TINYINT,heater TINYINT)')
        
    def writeData(self,dataObject):
        self.db.execute("insert into data values (null,(DATETIME(current_timestamp,'LOCALTIME')),?,?,?,?,?)",dataObject.getDict())
        self.db.commit()
        
    #def writePump(self,b):
        #self.db.execute("UPDATE pump SET eventtime = DATETIME(current_timestamp,'LOCALTIME'),status = ?",b)
        #self.db.commit()
        
    #def readPump(self):
        #cursor = self.db.execute('SELECT * FROM pump')
        #return cursor.fetchall()    
        
    def writeLog(self,logEntry):
        self.db.execute("insert into log values (null,(DATETIME(current_timestamp,'LOCALTIME')),'"+logEntry+"')")
        self.db.commit()    
        
class dataObject:
    def __init__(self):
        self.temp = 'na'
        self.humid = 'na'
        self.lights = 0
        self.pump = 0
        self.heater = 0
        
    def getDict(self):
        d = [self.temp,self.humid,self.lights,self.pump,self.heater]
        return d
        
