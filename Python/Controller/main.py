from texts import *
from initialize import *
from my_sql_class import *
#import RPi.GPIO as GPIO
import datetime,time,traceback
#import Adafruit_DHT

        
settings = readSettingsFile()
db = mySQLclass(settings.get('db'))
db.initializeDB()
db.writeLog("System started")
#sensor = Adafruit_DHT.DHT11
pins = MyPins(settings)
cycleLenght = int(str(settings.get('cycleLength')))
#Setting light pin lp and pump pin pp
lp = int(pins.lightPin)
pp = int(pins.pumpPin)
pumpStamp = int(datetime.datetime.now().timestamp() / 60)
pumpBool = True

def main():
        print(td[13])
        lightON = parseTime(settings.get('lightCycleON'))
        lightOFF = parseTime(settings.get('lightCycleOFF'))
        print('Light Cycle ON time set at: {}'.format(lightON))
        print('Light Cycle OFF time set at: {}'.format(lightOFF))
        data = dataObject()
        #pinSetup(lp,pp)

      
        while True:
                #Loop start timestamp
                ls = int(round(time.time()))
                
                #Process block starts here
                
                #data.lights = controlLightPins(lightON,lightOFF)
                data.pump = controlPump()
                
                #print(td[14])
                #humidity, temperature = Adafruit_DHT.read_retry(sensor, pins.DTHPin)
                #if humidity is not None and temperature is not None:
                        #print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
                        #data.temp,data.humid = str(temperature),str(humidity)
                        
                #else:
                        #print('Failed to get reading. Try again!')
                        
                
                db.writeData(data)
                #Process block ends here
                
                #Loop end timestamp
                le = int(round(time.time()))
                i=0
                rem = cycleLenght - (le -ls)
                print('Idle for {} seconds'.format(rem),end='')
                while (i < rem):
                        print('.',end='')
                        time.sleep(1)
                        i+=1
                print('')

                
#def pinSetup(lp,pp):
        #GPIO.setmode(GPIO.BCM)
        #GPIO.setwarnings(False)        
        #GPIO.setup(lp,GPIO.OUT, initial = GPIO.LOW)
        #GPIO.setup(pp,GPIO.OUT, initial = GPIO.HIGH)
        #print(td[15])
        #print(td[16].format(lp))
        #print(td[17].format(pp))
        
#def controlLightPins(lon,loff):
        
        #if(lon == loff):
                #if(not GPIO.input(lp)):
                        #GPIO.output(lp,GPIO.HIGH)
                        #db.writeLog("Lights switched on for 24/7")
                        #return 1
                #else:
                        #return 1
        
        #ct = datetime.datetime.now().time()
        
        #if(ct > lon and ct < loff):
                #if not(GPIO.input(lp)):
                        #GPIO.output(lp,GPIO.HIGH)
                        #db.writeLog("Lights switched on")
                        #print(td[18])
                        #return 1
                #else:
                        #return 1
        #else:
                #if (GPIO.input(lp)):
                        #GPIO.output(lp,GPIO.LOW)
                        #db.writeLog("Lights switched off")
                        #print(td[19])
                        #return 0
                #else:
                        #return 0
        

def controlPump():
        global pumpBool
        global pumpStamp
        if(pumpBool):
                if(int(datetime.datetime.now().timestamp() / 60) - pumpStamp >= int(settings.get('pumpDurOn'))):
                        print('Switching pump off...')
                        #GPIO.output(lp,GPIO.LOW)
                        db.writeLog("Pump switched off")
                        pumpStamp = int(datetime.datetime.now().timestamp() / 60)
                        pumpBool = False
                        return 0
                else:
                        return 1

        else:
                if(int(datetime.datetime.now().timestamp() / 60) - pumpStamp >= int(settings.get('pumpDurOff'))):
                        print('Switching pump on...')
                        #GPIO.output(lp,GPIO.HIGH)
                        db.writeLog("Pump switched on")
                        pumpStamp = int(datetime.datetime.now().timestamp() / 60)
                        pumpBool = True
                        return 1
                else:
                        return 0


def parseTime(t):
        split = t.split(':')
        timeObj = datetime.time(int(split[0]),int(split[1]),0)
        return timeObj

main()