td = {
    1:"db=database.db\nmaxTemp=30\nminTemp=20\nmaxHum=70\nminHum=40\npumpDurOn=5\npumpDurOff=60\nDTHPin=4\nlightPin=7\npumpPin=8\nfanPin=0\ncycleLength=60\nlightCycleON=06:00\nlightCycleOFF=18:00\nHELP Everything below is ingored and serves as a guide to configure the above.\n------------\nLight Cycle\n------------\nLight Cycle to be set in the following format each line per time setting: lightCycleON1=08:00\nlightCycleOFF1=20:00\nIf the clight cylce settings are left as default a 24/7 cycle will be applied.\n------------\nTemperatures.\n------------\nDuring Vegetative Stage 20-30.\nDuring FLowering 18-26.\n------------\nHumidity.\n------------\nDuring Vegetative 50-80.\nDuring Flowering 40-50."
    ,2:"settings.txt"
    ,3:"The value for {} has not been set in the settins.txt file, please set and start the application again.\nPress any keu to continue..."
    ,4:"Creating new settings file"
    ,5:"A new settings file has been created, please compelete the settings and start the application again.\nPress any keu to continue..."
    ,6:"Connecting to database..."
    ,7:"Database connected..."
    ,8:'Checking tables'
    ,9:'Database OK...'
    ,10:'Database Empty...'
    ,11:'Checking database and tables...'
    ,12:'Reading settings file...'
    ,13:'Starting main cycle...'
    ,14:'Reading Temperature and Humidity Sensor...'
    ,15:'Light and Pump pins initialized...'
    ,16:'Light pin set as pin {}...'
    ,17:'Pump pin set as pin {}...'
    ,18:'Lights ON...'
    ,19:'Lights OFF...'
}